module Potepan
  module ApplicationHelper
    def full_title(subtitle: "")
      title = "PotepanEC"
      if subtitle.present?
        "#{title} | #{subtitle}"
      else
        title
      end
    end
  end
end

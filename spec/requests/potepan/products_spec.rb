require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :request do
  describe "GET #show" do
    let(:product) { create(:product) }

    context "product exists" do
      it "returns status code 200" do
        get potepan_product_url product.id
        expect(response.status).to eq 200
      end
    end

    context "product does not exist" do
      it "returns ActiveRecord::RecordNotFound" do
        expect { get potepan_product_url 100 }.to raise_error ActiveRecord::RecordNotFound
      end
    end
  end
end

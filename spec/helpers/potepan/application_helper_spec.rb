require 'rails_helper'

RSpec.describe ApplicationHelper do
  include Potepan::ApplicationHelper

  describe "#full_title" do
    it "returns correct title" do
      expect(full_title).to eq "PotepanEC"
    end

    it "returns correct title when argument is specified" do
      expect(full_title(subtitle: "hoge")).to eq "PotepanEC | hoge"
    end
  end
end

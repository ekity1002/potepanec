require 'spree/testing_support/factories/shipping_category_factory'

FactoryGirl.define do
  factory :product, class: "Spree::Product" do
    # NOTE slug は create した時に自動で保存されるので設定していない
    sequence(:name) { |n| "Product ##{n} - #{Kernel.rand(9999)}" }
    price 1000
    cost_price 900
    description "example description"
    available_on { 1.year.ago }
    created_at { Time.now }
    shipping_category { |r| Spree::ShippingCategory.first || r.association(:shipping_category) }
  end
end

require 'rails_helper'

RSpec.feature 'Visiting Product', type: :feature do
  include Potepan::ApplicationHelper

  given(:product) { create(:product) }

  before do
    visit potepan_product_path(product.id)
  end

  describe "page contents" do
    it "has the correct title" do
      expect(page).to have_title(full_title(subtitle: product.name))
    end

    it "displays product name" do
      expect(page).to have_selector ".page-title", text: product.name
      expect(page).to have_selector "#product-name", text: product.name
      expect(page).to have_selector ".breadcrumb .active", text: product.name
    end

    it "displays product price" do
      price = "#{product.master.cost_price} #{product.master.cost_currency}"
      expect(page).to have_selector '#product-price', text: price
    end

    it "dislpays product description" do
      expect(page).to have_selector '#product-description', text: product.description
    end
  end
end
